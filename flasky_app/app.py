from Project import createApp
import unittest, sys, coverage,os

COV = None
if os.environ.get('FLASK_ENV') == 'development':
    COV = coverage.coverage(
        branch=True,
        include='Project/*',
        omit=[
            'Project/__init__.py',
            'Project/userTests/*',
            'Project/*/_tests/*'
        ]
    )
    COV.start()


app = createApp()

@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('Project', pattern='test_*.py')

    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        sys.exit(0)
    sys.exit(1)


@app.cli.command()
def cov():
    if COV is None:
        print('Running coverage on production environment')
        exit(0)
    tests = unittest.TestLoader().discover('Project', pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        COV.stop()
        COV.save()
        COV.report()
        COV.html_report()
        COV.erase()
        sys.exit(0)
    sys.exit(1)
