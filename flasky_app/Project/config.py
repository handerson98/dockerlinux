class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
            'postgres',
            'postgres',
            'postgres-db',
            '5432',
            'flask_example'
        )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = False


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}'.format(
        'postgres',
        'postgres',
        'databaseaws.cpek5v8cmdcs.us-east-2.rds.amazonaws.com',
        '5432',
        'userdocker'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
